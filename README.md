# table-test

## Build docker image
```
docker build -t table-test .
```

### Run docker image
```
docker run -it -p 8080:8080 --rm --name table-test table-test
```
### Open Browser
```
http://localhost:8080
```
